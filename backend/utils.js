function createResult(error, data) {
    const result = {}
    if (error) {
        result['status'] = 'error failed again'
        result['error'] = error
    } else {
        result['status'] = 'mayank ka data '
        result['data'] = data
    }
    return result
}

module.exports = {
    createResult,
}